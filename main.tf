locals {
  tags = merge(
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-route53"
    },
    var.tags
  )

  caller_origin_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

#####
# Hosted zone
#####

locals {
  delegation_sets = { for zone in var.public_zones : zone.logical_name => defaults(zone, {
    prefix = ""
  }) if zone.delegation_set == true }

  public_zones = { for zone in var.public_zones : zone.logical_name => defaults(zone, {
    prefix         = ""
    delegation_set = false
    dnssec         = true
  }) }

  private_zones = { for zone in var.private_zones : zone.logical_name => defaults(zone, {
    prefix = ""
  }) }

  existing_zones = { for zone in var.existing_zones : zone.logical_name => defaults(zone, {
    dnssec = false
  }) }

  zone_vpc_attachment_ids = concat(flatten([
    for zone in local.private_zones : [
      for vpc_id in coalesce(zone.vpc_attachment_ids, []) :
      {
        zone_id = lookup(aws_route53_zone.private, zone.logical_name).id
        vpc_id  = vpc_id
      }
    ]
    ]),
    flatten([
      for zone in var.existing_zones : [
        for vpc_id in coalesce(zone.vpc_attachment_ids, []) :
        {
          zone_id = zone.zone_id
          vpc_id  = vpc_id
        }
      ]
    ])
  )
}

resource "aws_route53_delegation_set" "this" {
  for_each = local.delegation_sets

  reference_name = each.key
}

resource "aws_route53_zone" "public" {
  for_each = local.public_zones

  name              = format("%s%s%s", var.prefix, lookup(each.value, "prefix"), lookup(each.value, "name"))
  comment           = lookup(each.value, "comment")
  delegation_set_id = lookup(each.value, "delegation_set") == true ? lookup(aws_route53_delegation_set.this, each.key).id : null

  tags = merge(
    local.tags,
    {
      documentation-link = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone"
      Name               = lookup(each.value, "name")
      Description        = lookup(each.value, "comment")
    },
    lookup(each.value, "tags"),
  )
}

resource "aws_route53_zone" "private" {
  for_each = local.private_zones

  name    = format("%s%s%s", var.prefix, lookup(each.value, "prefix"), lookup(each.value, "name"))
  comment = lookup(each.value, "comment")

  vpc {
    vpc_id = var.vpc_id
  }

  tags = merge(
    local.tags,
    {
      documentation-link = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone"
      Name               = each.key
      Description        = lookup(each.value, "comment")
    },
    lookup(each.value, "tags"),
  )

  lifecycle {
    ignore_changes = [vpc]
  }
}

resource "aws_route53_zone_association" "this" {
  count = length(local.zone_vpc_attachment_ids)

  zone_id = lookup(element(local.zone_vpc_attachment_ids, count.index), "zone_id")
  vpc_id  = lookup(element(local.zone_vpc_attachment_ids, count.index), "vpc_id")
}

#####
# DNSSEC
#####

locals {
  should_create_dnssec_kms_key = var.dnssec_enable == true && var.dnssec_external_key_enabled == false

  dnssec_zones_ids = concat(
    [
      for zone_logical_name, zone in local.public_zones :
      {
        zone_logical_name = zone.logical_name
        zone_id           = lookup(aws_route53_zone.public, zone.logical_name).id
      } if zone.dnssec == true
    ],
    [
      for zone_logical_name, zone in local.existing_zones :
      {
        zone_logical_name = zone.logical_name
        zone_id           = zone.zone_id
      } if zone.dnssec == true
    ],
  )

  dnssec_zones = { for zone in local.dnssec_zones_ids : zone.zone_logical_name => zone.zone_id }
}

resource "aws_kms_alias" "this_dnssec" {
  count = local.should_create_dnssec_kms_key ? 1 : 0

  name          = "alias/${var.dnssec_kms_key_alias_name}"
  target_key_id = aws_kms_key.this_dnssec.*.id[0]
}

resource "aws_kms_key" "this_dnssec" {
  count = local.should_create_dnssec_kms_key ? 1 : 0

  customer_master_key_spec = "ECC_NIST_P256"
  description              = var.dnssec_kms_key_description
  deletion_window_in_days  = var.dnssec_kms_key_deletion_window_in_days
  enable_key_rotation      = false
  key_usage                = "SIGN_VERIFY"

  policy = data.aws_iam_policy_document.this_dnssec.*.json[0]

  tags = merge(
    local.tags,
    {
      documentation-link = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key"
      Name               = var.dnssec_ksk_name
      Description        = var.dnssec_kms_key_description
    },
    var.dnssec_kms_key_tags,
  )
}

resource "aws_route53_key_signing_key" "this" {
  for_each = var.dnssec_enable ? local.dnssec_zones : {}

  hosted_zone_id             = each.value
  key_management_service_arn = local.should_create_dnssec_kms_key == true ? aws_kms_alias.this_dnssec.*.arn[0] : var.dnssec_external_kms_key_arn
  name                       = var.dnssec_ksk_name
}

resource "aws_route53_hosted_zone_dnssec" "this" {
  for_each = var.dnssec_enable ? local.dnssec_zones : {}

  hosted_zone_id = each.value

  depends_on = [
    aws_route53_key_signing_key.this
  ]
}

#####
# Records
#####

locals {
  records = { for record in var.records : format("%s_%s_%s", record.logical_name, record.zone_logical_name, record.type) => record }

  alias_records = { for record in var.alias_records : format("%s_%s_%s", record.logical_name, record.zone_logical_name, record.type) => defaults(record, {
    alias_evaluate_target_health = true
  }) }

  all_zones_ids = merge(
    {
      for zone in var.private_zones : zone.logical_name => lookup(aws_route53_zone.private, zone.logical_name).zone_id
    },
    {
      for zone in var.public_zones : zone.logical_name => lookup(aws_route53_zone.public, zone.logical_name).zone_id
    },
    {
      for zone in var.existing_zones : zone.logical_name => zone.zone_id
    },
  )
}

resource "aws_route53_record" "this" {
  for_each = local.records

  zone_id         = lookup(local.all_zones_ids, each.value.zone_logical_name)
  name            = format("%s%s", var.prefix, each.value.name)
  type            = each.value.type
  ttl             = each.value.ttl
  records         = each.value.records
  allow_overwrite = true
}

resource "aws_route53_record" "this_alias" {
  for_each = local.alias_records

  zone_id         = lookup(local.all_zones_ids, each.value.zone_logical_name)
  name            = format("%s%s", var.prefix, each.value.name)
  type            = each.value.type
  allow_overwrite = true

  alias {
    name                   = each.value.alias_name
    zone_id                = each.value.alias_zone_id
    evaluate_target_health = each.value.alias_evaluate_target_health
  }
}
