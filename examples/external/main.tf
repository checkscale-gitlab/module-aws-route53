#####
# Randoms
#####

resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

resource "random_integer" "this" {
  min = 4
  max = 254
}

resource "random_integer" "this2" {
  min = 1
  max = 99
}

#####
# Baseline
#####

data "aws_vpc" "default" {
  default = true
}

data "aws_iam_policy_document" "dnssec" {
  statement {
    sid    = "Read permissions for everyone dont do that in prod"
    effect = "Allow"
    actions = [
      "kms:*",
    ]
    resources = [
      "*"
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

resource "aws_kms_key" "example" {
  customer_master_key_spec = "ECC_NIST_P256"
  deletion_window_in_days  = 7
  key_usage                = "SIGN_VERIFY"

  policy = data.aws_iam_policy_document.dnssec.json
}

resource "aws_vpc" "main" {
  cidr_block = format("10.%s.%s.0/24", random_integer.this.result, random_integer.this2.result)
}

resource "aws_route53_zone" "private" {
  name = "${random_string.this.result}.private.tftest.com"

  vpc {
    vpc_id = data.aws_vpc.default.id
  }

  lifecycle {
    ignore_changes = [vpc]
  }
}

resource "aws_route53_zone" "public" {
  name = "${random_string.this.result}.public.tftest.com"
}

#####
# External example
# Shows how to:
# - add records to existing public/private zones
# - link VPC to existing private zone
# - enable DNSSEC on existing public zone
# - use an external KMS key for DNSSEC
#####

module "external" {
  source = "../../"

  prefix = random_string.this.result

  existing_zones = [
    {
      logical_name       = "private.tftest.com"
      zone_id            = aws_route53_zone.private.zone_id
      vpc_attachment_ids = [aws_vpc.main.id]
    },
    {
      logical_name = "public.tftest.com"
      zone_id      = aws_route53_zone.public.zone_id
      dnssec       = true
    },
  ]

  #####
  # DNSSEC
  #####

  dnssec_enable               = true
  dnssec_ksk_name             = "${random_string.this.result}ksktftest"
  dnssec_external_key_enabled = true
  dnssec_external_kms_key_arn = aws_kms_key.example.arn

  #####
  # Records
  #####

  records = [
    {
      zone_logical_name = "public.tftest.com"
      name              = "record"
      logical_name      = "record"
      type              = "A"
      ttl               = 3600
      records           = [format("1.2.3.%s", random_integer.this.result)]
    },
    {
      zone_logical_name = "private.tftest.com"
      name              = "record"
      logical_name      = "record"
      type              = "A"
      ttl               = 80
      records           = [format("1.2.3.%s", random_integer.this.result)]
    },
  ]
}
