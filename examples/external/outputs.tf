#####
# external example
#####

output "external_delegation_set_ids" {
  value = module.external.delegation_set_ids
}

output "external_delegation_set_arns" {
  value = module.external.delegation_set_arns
}

output "external_delegation_set_name_servers" {
  value = module.external.delegation_set_name_servers
}

output "external_public_zone_arns" {
  value = module.external.public_zone_arns
}

output "external_public_zone_ids" {
  value = module.external.public_zone_ids
}

output "external_public_zone_domain_names" {
  value = module.external.public_zone_domain_names
}

output "external_public_zone_name_servers" {
  value = module.external.public_zone_name_servers
}

output "external_private_zone_arns" {
  value = module.external.private_zone_arns
}

output "external_private_zone_ids" {
  value = module.external.private_zone_ids
}

output "external_private_zone_domain_names" {
  value = module.external.private_zone_domain_names
}

output "external_private_zone_name_servers" {
  value = module.external.private_zone_name_servers
}

output "external_dnssec_ksk_ids" {
  value = module.external.dnssec_ksk_ids
}

output "external_dnssec_ksk_records" {
  value = module.external.dnssec_ksk_records
}

output "external_dnssec_ksk_ds_records" {
  value = module.external.dnssec_ksk_ds_records
}

output "external_dnssec_ksk_public_keys" {
  value = module.external.dnssec_ksk_public_keys
}

output "external_dnssec_ksk_digest_values" {
  value = module.external.dnssec_ksk_digest_values
}

output "external_dnssec_ksk_digest_algorithm_types" {
  value = module.external.dnssec_ksk_digest_algorithm_types
}

output "external_dnssec_ksk_digest_algorithm_mnemonics" {
  value = module.external.dnssec_ksk_digest_algorithm_mnemonics
}

output "external_dnssec_ksk_signing_algorithm_type" {
  value = module.external.dnssec_ksk_signing_algorithm_type
}

output "external_dnssec_kms_key_id" {
  value = module.external.dnssec_kms_key_id
}

output "external_dnssec_kms_key_arn" {
  value = module.external.dnssec_kms_key_arn
}

output "external_dnssec_kms_key_alias_arn" {
  value = module.external.dnssec_kms_key_alias_arn
}

output "external_resolver_inbound_security_group_ids" {
  value = module.external.resolver_inbound_security_group_ids
}

output "external_resolver_inbound_ids" {
  value = module.external.resolver_inbound_ids
}

output "external_resolver_inbound_arns" {
  value = module.external.resolver_inbound_arns
}

output "external_resolver_inbound_host_vpc_ids" {
  value = module.external.resolver_inbound_host_vpc_ids
}

output "external_resolver_outbound_security_group_id" {
  value = module.external.resolver_outbound_security_group_id
}

output "external_resolver_outbound_ids" {
  value = module.external.resolver_outbound_ids
}

output "external_resolver_outbound_arns" {
  value = module.external.resolver_outbound_arns
}

output "external_resolver_outbound_host_vpc_ids" {
  value = module.external.resolver_outbound_host_vpc_ids
}

output "external_rule_forward_ids" {
  value = module.external.rule_forward_ids
}

output "external_rule_forward_arns" {
  value = module.external.rule_forward_arns
}

output "external_rule_forward_owner_ids" {
  value = module.external.rule_forward_owner_ids
}

output "external_rule_forward_share_statuses" {
  value = module.external.rule_forward_share_statuses
}

output "external_rule_association_forward_id" {
  value = module.external.rule_association_forward_id
}

output "external_rule_forward_share_ids" {
  value = module.external.rule_forward_share_ids
}

output "external_rule_forward_share_arns" {
  value = module.external.rule_forward_share_arns
}

output "external_resource_association_forward_id" {
  value = module.external.resource_association_forward_id
}

output "external_principal_association_forward_id" {
  value = module.external.principal_association_forward_id
}

output "external_record_names" {
  value = module.external.record_names
}

output "external_record_fqdns" {
  value = module.external.record_fqdns
}
