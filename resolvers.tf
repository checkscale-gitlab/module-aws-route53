#####
# Resolver endpoints inbound
#####

resource "aws_route53_resolver_endpoint" "this_inbound" {
  count = var.resolver_inbound_count

  name      = element(var.resolver_inbound_names, count.index)
  direction = "INBOUND"

  security_group_ids = [
    element(aws_security_group.this_inbound.*.id, count.index),
  ]

  // This must be computed dynamically when transforming 0.11 in 0.12
  ip_address {
    ip        = element(var.resolver_inbound_ip_addresses[count.index], 0)
    subnet_id = element(var.resolver_inbound_subnet_ids[count.index], 0)
  }

  ip_address {
    ip        = element(var.resolver_inbound_ip_addresses[count.index], 1)
    subnet_id = element(var.resolver_inbound_subnet_ids[count.index], 1)
  }

  tags = merge(
    {
      "Terraform" = "true"
    },
    {
      "Name" = element(var.resolver_inbound_names, count.index)
    },
    var.tags,
    var.resolver_tags,
  )
}

#####
# Resolver endpoints outbound
#####

resource "aws_route53_resolver_endpoint" "this_outbound" {
  count = var.resolver_outbound_count

  name      = element(var.resolver_outbound_names, count.index)
  direction = "OUTBOUND"

  security_group_ids = [
    element(aws_security_group.this_outbound.*.id, count.index),
  ]

  // This must be computed dynamically when transforming 0.11 in 0.12
  ip_address {
    ip        = element(var.resolver_outbound_ip_addresses[count.index], 0)
    subnet_id = element(var.resolver_outbound_subnet_ids[count.index], 0)
  }

  ip_address {
    ip        = element(var.resolver_outbound_ip_addresses[count.index], 1)
    subnet_id = element(var.resolver_outbound_subnet_ids[count.index], 1)
  }

  tags = merge(
    {
      "Terraform" = "true"
    },
    {
      "Name" = element(var.resolver_outbound_names, count.index)
    },
    var.tags,
    var.resolver_tags,
  )
}

#####
# Forward rules
#####

resource "aws_route53_resolver_rule" "this_forward" {
  count = var.rule_forward_count > 0 && var.rule_forward_attachment_ids_count == 0 ? var.rule_forward_count : 0

  domain_name = element(var.rule_forward_domain_names, count.index)
  name        = element(var.rule_forward_names, count.index)
  rule_type   = "FORWARD"
  resolver_endpoint_id = element(concat(var.rule_forward_resolver_endpoint_ids, [""]), 0) != "" ? element(
    concat(var.rule_forward_resolver_endpoint_ids, [""]),
    count.index,
    ) : element(
    concat(aws_route53_resolver_endpoint.this_outbound.*.id, [""]),
    0,
  )

  dynamic "target_ip" {
    for_each = [
      for ip in var.rule_forward_resolver_target_ips[tostring(count.index)] : ip
    ]

    content {
      ip = target_ip.value
    }
  }

  tags = merge(
    {
      "Terraform" = "true"
    },
    {
      "Name" = element(var.rule_forward_names, count.index)
    },
    var.tags,
    var.rule_forward_tags,
  )
}

resource "aws_route53_resolver_rule_association" "this_forward" {
  count = var.rule_forward_count > 0 ? var.rule_forward_vpc_attachment_count * var.rule_forward_count : 0

  resolver_rule_id = element(concat(var.rule_forward_attachment_ids, [""]), 0) == "" ? element(
    concat(aws_route53_resolver_rule.this_forward.*.id, [""]),
    count.index % var.rule_forward_count,
    ) : element(
    concat(var.rule_forward_attachment_ids, [""]),
    count.index % var.rule_forward_count,
  )
  vpc_id = element(
    concat(var.rule_forward_vpc_attachment_ids),
    floor(count.index / var.rule_forward_count) % var.rule_forward_vpc_attachment_count,
  )
}

#####
# Resource share
#####

resource "aws_ram_resource_share" "this_forward" {
  count = length(var.rule_forward_share_indexes) > 0 ? length(var.rule_forward_share_indexes) : 0

  name                      = element(var.rule_forward_share_names, count.index)
  allow_external_principals = true

  tags = merge(
    {
      "Terraform" = "true"
    },
    {
      "Name" = element(var.rule_forward_share_names, count.index)
    },
    var.tags,
    var.rule_forward_share_tags,
  )
}

resource "aws_ram_resource_association" "this_forward" {
  count = length(var.rule_forward_share_indexes) > 0 ? length(var.rule_forward_share_indexes) : 0

  resource_arn = element(
    aws_route53_resolver_rule.this_forward.*.arn,
    element(var.rule_forward_share_indexes, count.index),
  )
  resource_share_arn = element(aws_ram_resource_share.this_forward.*.arn, count.index)
}

resource "aws_ram_principal_association" "this_forward" {
  count = length(var.rule_forward_share_indexes) > 0 && var.rule_forward_share_principal_count > 0 ? length(var.rule_forward_share_indexes) * var.rule_forward_share_principal_count : 0

  principal = element(
    var.rule_forward_share_principals,
    count.index % var.rule_forward_share_principal_count,
  )
  resource_share_arn = element(
    aws_ram_resource_share.this_forward.*.arn,
    floor(count.index / var.rule_forward_share_principal_count) % length(var.rule_forward_share_indexes),
  )
}
