data "aws_subnet" "this_inbound" {
  count = var.resolver_inbound_count

  id = element(var.resolver_inbound_subnet_ids[count.index], 0)
}

data "aws_subnet" "this_outbound" {
  count = var.resolver_outbound_count

  id = element(var.resolver_outbound_subnet_ids[count.index], 0)
}

data "aws_caller_identity" "current" {}

#####
# DNSSEC
#####

data "aws_iam_policy_document" "this_dnssec" {
  count = local.should_create_dnssec_kms_key ? 1 : 0

  statement {
    sid     = "TerraformAdminAccess"
    effect  = "Allow"
    actions = ["kms:*"]
    resources = [
      "*"
    ]
    principals {
      type = "AWS"
      identifiers = [
        local.caller_origin_arn,
      ]
    }
  }

  statement {
    sid    = "Read permissions for everyone"
    effect = "Allow"
    actions = [
      "kms:Get*",
      "kms:Describe*",
      "kms:List*",
    ]
    resources = [
      "*"
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }

  statement {
    sid    = "Allow Route 53 DNSSEC Service"
    effect = "Allow"
    actions = [
      "kms:DescribeKey",
      "kms:GetPublicKey",
      "kms:Sign",
    ]
    resources = [
      "*"
    ]
    principals {
      type = "Service"
      identifiers = [
        "dnssec-route53.amazonaws.com"
      ]
    }
  }

  statement {
    sid    = "Allow Route 53 DNSSEC Service to CreateGrant"
    effect = "Allow"
    actions = [
      "kms:CreateGrant",
    ]
    resources = [
      "*"
    ]
    principals {
      type = "Service"
      identifiers = [
        "dnssec-route53.amazonaws.com"
      ]
    }
    condition {
      test     = "Bool"
      values   = ["true"]
      variable = "kms:GrantIsForAWSResource"
    }
  }
}
